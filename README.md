Workshop Live Vektor Skizzen
=========

- Kritzeln als Lebenselexier
- Vorzeichnung, Umrisse, Flächen (Shape) (ohne vorzeichung)
- alchemy [^2] -> chaos & evolution [^1]
- Blender Grease Pencil [^4]
- live sketches - vector
	lebendige Eindrücke festhalten

warum?
------

- web
- pdf
- Qualität - insbesondere auf kleinen displays

geschichte des bilds (Linien in der Reihenfolge des Entstehens gespeichert)
- Abstrakter
- Skizzen näher an der Idee
- Skizzenbasierte Modellierung

Style: inspiriert von Aquarellen:
---------------------------------
- Transparente Shapes

- Linen

- Finger (Mehrbortiger Pinsel)

-> Variation/ Zufall
-> Farbharmonien[^5] HSV Farbraum

Witere Features:

- Morphen

- Kein Undo, Kein rotieren etc. obwohl in Vectorgrafiken leicht zu implememtieren
	
- Ein Go

Zielplattform: Pandora

Weitere Inspiration:[^6][^3][^7][^8]

Prototypen umgesetzt mit[^10]:
--------------

- Cairo (in C) erster Prototyp

- Godot für 3D [ZIP im Interetarchive](https://archive.org/details/gdvectorpaint291223)

--> Polygone statt Kurven

- Rust [egui](https://github.com/emilk/egui), [raqote](https://docs.rs/raqote/latest/raqote/)

-> hoffentlich android

Blender nicht auf Embedded Geräten (OpenGL 3.3)

Zeichenspiele
--------------
- linkshändig

- zeitbeschränkung

- blindzeichnen

- oneliner

- Style: shapes, Falschfarben, pinselborsten Transparenz, outlines

- Urban Sketcher [^9]


Handout
===========================
![Handout](Handout.svg "Handout")

Referenzen
==========================

[^1]: [Tutorial zu Kreativem Malen](https://www.davidrevoy.com/article172/chaosevolution-digital-painting-1h30-open-tutorial)

[^2]: [Vector Malprogramm für Kreativität](http://al.chemy.org/)

[^3]: [Artikel zum Prozeduralen Malen](https://shahriyarshahrabi.medium.com/procedural-paintings-with-genetic-evolution-algorithm-6838a6e64703)

[^4]: [Blender Grease Pencil](https://studio.blender.org/training/grease-pencil-fundamentals/pages/resources/)

[^5]: [Farbharmonien](https://de.wikipedia.org/wiki/Farbschema)

[^6]: [Code zum Prozeduralen Pinseln](https://github.com/IRCSS/Procedural-painting)

[^7]: ["How to Hack a Painting" by Tyler Hobbs;Strange Loop Conference](https://www.youtube.com/watch?v=5R9eywArFTE)
 
[^8]: [Autorin für Prozedurales Malen](https://web.archive.org/web/20180613080619/https://www.anastasiaopara.com/)
 
[^9]: [Urban Sketcher](https://urbansketchers.org/de/)

[^10]: [Mein Code](https://gitlab.com/dronn)
